import Dependencies._

ThisBuild / scalaVersion     := "2.10.3"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "io.github.drmanganese"
ThisBuild / organizationName := "schemamaker"

lazy val root = (project in file("."))
  .settings(
    name := "schemamaker",
    libraryDependencies += scalaTest % Test,
    libraryDependencies += scalavro
  )



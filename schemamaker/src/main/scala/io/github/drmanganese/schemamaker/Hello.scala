package schemamaker

import com.gensler.scalavro.types.AvroType
import scala.util.{ Try, Success, Failure }

object Hello extends App {
  val types = AvroType[grass_pokemon_value]

  println(types.schema)
}

case class grass_pokemon_value(id: Int, name: String, types: Array[TypeSlot])

case class TypeSlot(slot: Int, `type`: Type)

case class Type(name: String, url: String)
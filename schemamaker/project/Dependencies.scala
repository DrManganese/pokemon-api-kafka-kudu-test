import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val scalavro = "com.gensler" %% "scalavro" % "0.6.2"
}

# Lenses Box
docker run -d \
    --name "lenses_box" \
    -e ADV_HOST=127.0.0.1 \
    -e EULA="https://dl.lenses.stream/d/?id=df0d79a5-dfa1-44ea-88ff-03a033dabd38" \
    -p 3030:3030 \
    -p 9092:9092 \
    -p 2181:2181 \
    -p 8081:8081 \
    landoop/kafka-lenses-dev

# Kudu
docker run -d \
    -p 8050:8050 \
    -p 8051:8051 \
    -p 7050:7050 \
    -p 7051:7051 \
    usuresearch/kudu-docker-slim
